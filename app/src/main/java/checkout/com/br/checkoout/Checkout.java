package checkout.com.br.checkoout;

import android.content.Context;
import android.net.Uri;
import com.paypal.android.sdk.payments.PayPalConfiguration;

class Checkout {
    private static final String PUBLIC_KEY = "";//CLIENT ID OBTIDO NO SITE DO PAYPAL
    public static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_NO_NETWORK;// TODO CONFIGURAÇÃO DO AMBIENTE = ENVIRONMENT_SANDBOX / ENVIRONMENT_PRODUCTION
    public static final int REQUEST_CODE_PAYMENT = 1;
    public static final int REQUEST_CODE_FUTURE_PAYMENT = 2;
    public static final int REQUEST_CODE_PROFILE_SHARING = 3;
    public static PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(PUBLIC_KEY)
            //.sandboxUserPassword("")
            //
            .merchantName("");//NOME DO VENDEDOR
            //.merchantPrivacyPolicyUri(Uri.parse("https://www.example.com/privacy"))
            //.merchantUserAgreementUri(Uri.parse("https://www.example.com/legal"));
}

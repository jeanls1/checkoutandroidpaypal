package checkout.com.br.checkoout;

import java.io.Serializable;

public class Response implements Serializable{
    private String id, state;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}

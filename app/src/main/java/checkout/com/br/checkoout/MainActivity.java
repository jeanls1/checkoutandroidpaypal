package checkout.com.br.checkoout;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.paypal.android.sdk.payments.*;
import org.json.JSONException;

import java.math.BigDecimal;

import static checkout.com.br.checkoout.Checkout.*;

public class MainActivity extends AppCompatActivity {
    private Button pagar;
    private EditText editItem, editPrice;
    private static final String TAG = "pagamento";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent intent = new Intent(this, PayPalService.class);//Criação da intent do paypal para realizar o pagamento
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, Checkout.config);
        startService(intent);
        initViews();
    }

    private void initViews(){
        editItem = (EditText)findViewById(R.id.edit_product);
        editPrice = (EditText)findViewById(R.id.edit_price);
        pagar = (Button)findViewById(R.id.btn_pagar);
        pagar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String productName = editItem.getText().toString();
                String productPrice = editPrice.getText().toString();
                if(productName.length() == 0 || productPrice.length() == 0){
                    Toast.makeText(MainActivity.this, "Campos vazios", Toast.LENGTH_SHORT).show();
                    if(productName.length() == 0){
                        editItem.setError("Preencha o nome do item");
                    }
                    if(productPrice.length() == 0){
                        editPrice.setError("Preencha o valor do item");
                    }
                    return;
                }
                PayPalPayment itemToBuy = getItem(PayPalPayment.PAYMENT_INTENT_SALE, productName, new BigDecimal(productPrice));//Início da criação do pagamento passando um produto
                Intent intent = new Intent(MainActivity.this, PaymentActivity.class);
                intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, Checkout.config);
                intent.putExtra(PaymentActivity.EXTRA_PAYMENT, itemToBuy);//Envia o produto pra activity do paypal
                startActivityForResult(intent, REQUEST_CODE_PAYMENT);//inicia a activity do paypal e aguarda o seu callback onActivityResult para ver o restante
            }
        });
    }

    private PayPalPayment getItem(String paymentIntent, String nome, BigDecimal valor) {
        return new PayPalPayment(valor, "BRL", nome,
                paymentIntent);
    }

    private void displayResultText(String result) {
        Toast.makeText(
                getApplicationContext(),
                result, Toast.LENGTH_LONG)
                .show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm =
                        data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                        Log.i(TAG, confirm.toJSONObject().toString(4));
                        Log.i(TAG, confirm.getPayment().toJSONObject().toString(4));
                        sendAuthorizationToServer(confirm.toJSONObject().toString(4), confirm.getPayment().toJSONObject().toString(4));//Aqui é o método que fornece um retorno para seu servidor
                        displayResultText("Pagamento enviado ao paypal");
                    } catch (JSONException e) {
                        Log.e(TAG, "JSON ERROR ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i(TAG, "FALHA NA COMPRA.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(
                        TAG,
                        "PAGAMENTO OU CONFIGURAÇÃO INVÁLIDA");
            }
        } else if (requestCode == REQUEST_CODE_FUTURE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PayPalAuthorization auth =
                        data.getParcelableExtra(PayPalFuturePaymentActivity.EXTRA_RESULT_AUTHORIZATION);
                if (auth != null) {
                    try {
                        Log.i("FuturePaymentExample", auth.toJSONObject().toString(4));

                        String authorization_code = auth.getAuthorizationCode();
                        Log.i("FuturePaymentExample", authorization_code);

                        sendAuthorizationToServer(auth);
                        displayResultText("RECEBEU NO PAYPAL");

                    } catch (JSONException e) {
                        Log.e("FuturePaymentExample", "Uma falha ocorreu ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("FuturePaymentExample", "USUÁRIO CANCELOU");
            } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(
                        "FuturePaymentExample",
                        "NÃO FOI POSSÍVEL INICIAR O SERVIÇO DO PAYPAL");
            }
        } else if (requestCode == REQUEST_CODE_PROFILE_SHARING) {
            if (resultCode == Activity.RESULT_OK) {
                PayPalAuthorization auth =
                        data.getParcelableExtra(PayPalProfileSharingActivity.EXTRA_RESULT_AUTHORIZATION);
                if (auth != null) {
                    try {
                        Log.i("ProfileSharingExample", auth.toJSONObject().toString(4));

                        String authorization_code = auth.getAuthorizationCode();
                        Log.i("ProfileSharingExample", authorization_code);

                        sendAuthorizationToServer(auth);
                        displayResultText("Profile Sharing ENVIADO AO PAYPAL");

                    } catch (JSONException e) {
                        Log.e("ProfileSharingExample", "ERRO OCORRIDO: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("ProfileSharingExample", "O USUÁRIO CANCELOU.");
            } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i(
                        "ProfileSharingExample",
                        "NÃO FOI POSSÍVEL INICAR O SERVIÇO OU CONFIGURAÇÃO INVÁLIDA");
            }
        }
    }

    private void sendAuthorizationToServer(String confirmJSON, String paymentJSON){//Métodos responsáveis para realizar o envio das informações
        Log.i("AUTH", confirmJSON + "\n" + paymentJSON);//ESSE É O MÉTODO MAIS CHAMADO

        /*TODO confirmJSON retorna um JSON dessa forma:
         *
            {
                "client": {
                    "environment": "mock",
                    "paypal_sdk_version": "2.15.3",
                    "platform": "Android",
                    "product_name": "PayPal-Android-SDK"
                },
                "response": {
                    "create_time": "2014-02-12T22:29:49Z",
                    "id": "PAY-6RV70583SB702805EKEYSZ6Y", //TODO ID DA COMPRA
                    "intent": "sale",
                    "state": "approved"
                },
                "response_type": "payment"
            }
         */
        /*TODO O paymentJSON retorna:
        {
            "amount": "20",
            "currency_code": "BRL",
            "short_description": "fhfhf",
            "intent": "sale"
        }

         */
    }

    private void sendAuthorizationToServer(PayPalAuthorization authorization) {
        Log.i("AUTH", authorization.toString());
    }
}
